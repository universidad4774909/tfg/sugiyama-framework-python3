import itertools
import math
import string
import networkx as nx
import random
from typing import Dict, Iterable, List, Union
import matplotlib.pyplot as plt
from scipy.interpolate import make_interp_spline, BSpline
import copy
from typing import List, Tuple
import networkx as nx
import numpy as np
# Definición de un grafo dirigido usando NetworkX DiGraph
G = nx.DiGraph()

# 1. Análisis de ciclos, construye sucesión con el menor número de backedges.
#    (a) Guarda fuentes y sumideros en dos listas diferentes
#    (b) Elimina nodos fuente sucesivamente y los agrega a la lista de fuentes
#    (c) Elimina nodos sumidero sucesivamente y los agrega a la lista de sumideros
#    (d) Elige el siguiente candidato según el rango de entrada y salida del nodo, rangOut = maxNode([R_out(v) - R_in(v) para v en G])
#        (e) Elimina v del grafo y lo agrega a la lista de fuentes

def removeNode(G: nx.DiGraph, node: str):
    if node in G:
        G.remove_node(node)
 
def cycleAnalysis(G: nx.DiGraph) -> List:
    N: nx.DiGraph = copy.deepcopy(G)  # El grafo será modificado... se copia el grafo
    Sl: List = []  # Sl, Sr en Sugiyama et. al.
    Sr: List = []

    while N:  # mientras N no esté vacío
        sources = [n for n in N if N.in_degree(n) == 0]
        sinks = [n for n in N if N.out_degree(n) == 0]

        # (b)
        if sources:
            Sl += sources

            for n in sources:
                removeNode(N, n)  # elimina todos los sumideros

        # (c)
        elif sinks:
            Sr += sinks

            for n in sinks:
                removeNode(N, n)  # elimina todos los sumideros

        # (d)
        elif N:
            o = max(N, key=lambda n: N.out_degree(n) - N.in_degree(n))  # obtener el nodo con el máximo rangOut
            # (e)
            Sl += [o]
            removeNode(N, o)

    return Sl + Sr

def twistEdges(G: nx.DiGraph, B: List[Tuple]) -> None:
    for u, v in B:
        G.remove_edge(u, v)
        G.add_edge(v, u)

def invertBackEdges(G: nx.DiGraph, S: List) -> nx.DiGraph:
    N: nx.DiGraph = copy.deepcopy(G)  # El grafo será modificado... se copia el grafo
    B: List[Tuple] = []  # backedges
    for i, n in enumerate(S):

        C = list(N.successors(n))  # children
        for c in C:
            j = S.index(c)
            if j < i:
                B += [(n, c)]

    twistEdges(N, B)

    return N


# 2. Asigna cada nodo a un nivel horizontal
#    (a) Determina los sumideros
#    (b) Asigna los sumideros a un nuevo nivel
#    (c) Elimina todos los sumideros del grafo

# Toma un grafo acíclico
def levelAssignment(G:nx.DiGraph):
    N = copy.deepcopy(G)  # Se copia el grafo para no modificar el original

    L = []  # niveles

    while N:  # Mientras N no esté vacío
        # (a) Encuentra los sumideros (nodos sin sucesores)
        sinks = [n for n in N if N.out_degree(n) == 0]
    
        # (c) Agrega los sumideros al nivel actual y luego los elimina del grafo
        L += [sinks]  # Por ejemplo: L = []; L += [['A', 'B', 'C']] => L = [['A', 'B', 'C']]
        
        # (b) Elimina los sumideros del grafo
        for n in sinks:
            removeNode(N, n)

    # Devuelve los niveles en orden inverso (de abajo hacia arriba)
    return [x for x in reversed(L)]

def removeEdges(N: nx.DiGraph, edges_to_remove: List[tuple]) -> None:
    """
    Elimina las aristas especificadas de un grafo DiGraph.

    Args:
    - N: El grafo DiGraph sobre el que se eliminarán las aristas.
    - edges_to_remove: Lista de tuplas que representan las aristas a eliminar.
    """
    for edge in edges_to_remove:
        a, b = edge
        if N.has_edge(a, b):  # Comprueba si la arista existe en el grafo
            N.remove_edge(a, b)


# Toma (a, b) y si hay un nivel entre ellos (donde no están conectados):
#   toma a como el nodo de origen,
#   crea un nuevo nodo
#     conecta ese nuevo nodo a a
#   conecta ese nuevo nodo a b
# para múltiples niveles intermedios
def solveMidTransition(N: nx.DiGraph, edge: Tuple[str, str], L: List[List[str]], node2Level: Dict[str,int]) -> Tuple[Dict[str, dict], List[List[str]]]:
    (a, b) = edge

    lfrom = node2Level[a]
    lto = node2Level[b]

    # Determina la dirección del paso según los niveles de los nodos
    if lto < lfrom:
        step = -1
    else:
        step = 1

    # Elimina la arista original entre los nodos a y b
    removeEdges(N, [(a, b)])
    between = set()
    u = a  # (u, v)
    for i in range(lfrom + step, lto, step):  # para niveles entre a y b
        v = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(3))
        N.add_edge(u, v)
        between.add(v)
        L[i] += [v]  # Agrega al nivel
        u = v

    # Conecta el último nodo creado a b
    N.add_edge(u,b)

    return N, L,between

# Obtiene los nodos intermedios entre niveles
def getInBetweenNodes(G: nx.DiGraph, L: List[List[str]]) -> Tuple[Dict[str, dict], List[List[str]], Dict[str, int]]:
    # Realiza una copia profunda del grafo dirigido G
    N = copy.deepcopy(G)
    # Diccionario para almacenar el nivel de cada nodo
    node_levels = {}  # type: Dict[str, int]

    # Asigna un nivel a cada nodo y almacena en node_levels
    for i, l in enumerate(L):
        for j in l:
            node_levels[j] = i

    IB = []

    # Encuentra los nodos intermedios entre niveles
    for nLevel, l in enumerate(L):
        for n in l:
            node = N.nodes[n]
            for c in N.successors(n):
                cLevel = node_levels.get(c, -1)  # Obtiene el nivel de c desde node_levels
                dif = abs(cLevel - nLevel)
                if dif > 1:
                    IB.append((n, c))
    total_between = []
    # Resuelve las transiciones intermedias entre nodos
    for (n, c) in IB:
        N,L,between = solveMidTransition(N, (n, c), L,node_levels)  
        total_between+=between
    return N, L, node_levels, total_between

# Convierte los elementos de la lista A en índices basados en la lista B
def toIndex(A: List[str], B: List[str]) -> Iterable[int]:
    for a in A:
        if a in B:
            yield B.index(a)

def costMatrix(N: nx.DiGraph, L1: List[str], L2: List[str]) -> List[List[int]]:
    """
    Calcula la matriz de costos para minimizar el cruce entre dos niveles.

    Args:
    - N: El grafo DiGraph.
    - L1: Lista de nodos que representan un nivel del grafo.
    - L2: Lista de nodos que representan otro nivel del grafo.

    Returns:
    - Una lista de listas que representa la matriz de costos.
    """
    M = [[0 for _ in L1] for _ in L1]

    for ((ui, u), (vi, v)) in itertools.combinations(enumerate(L1), 2):
        Eu = list(toIndex(list(N.successors(u)) + list(N.predecessors(u)), L2))
        Ev = list(toIndex(list(N.successors(v)) + list(N.predecessors(v)), L2))

        for uc_i, vc_i in itertools.product(Eu, Ev):
            if uc_i > vc_i:
                M[ui][vi] += 1
            if uc_i < vc_i:
                M[vi][ui] += 1

    return M

def crossSort(A: List[int], M: List[List[int]]) -> List[int]:
    """
    Ordena los nodos en A para minimizar el cruce de aristas según la matriz de costos M.

    Args:
    - A: Lista de índices de nodos.
    - M: Matriz de costos.

    Returns:
    - Lista de índices de nodos ordenados para minimizar el cruce de aristas.
    """
    if len(A) < 2:
        return A

    p = len(A) // 2

    L = crossSort(A[:p], M)
    R = crossSort(A[p:], M)

    S = []
    li = ri = 0
    while li < len(L) and ri < len(R):
        l = L[li]
        r = R[ri]

        if M[l][r] <= M[r][l]:
            S.append(l)
            li += 1
        else:
            S.append(r)
            ri += 1

    S += L[li:]
    S += R[ri:]

    return S

def twoLevelCrossMin(N: nx.DiGraph, Levels: List[List[str]],a,b) -> List[List[str]]:
    """
    Implementa el algoritmo de ordenamiento de cruces de dos niveles.

    Args:
    - N: El grafo DiGraph.
    - Levels: Lista de listas que representan los niveles del grafo.

    Returns:
    - Lista de listas de nodos ordenados para minimizar los cruces de aristas entre los niveles.
    """
    Lvl = [x for x in Levels]    # copia la lista, solo para asegurarse...

    R = [Levels[-1]]  # toma la primera entrada, no se modificará

    B = Lvl.pop()
    while Lvl:
        T = Lvl.pop()

        M = costMatrix(N, T, B)
        T_i = crossSort(list(range(len(T))), M)       # no es B = T, sino B = T ordenado => B = recMinCross(T, M), se usa como base para la siguiente iteración
        B = [T[i] for i in T_i]

        R.append(B) # agrega la permutación con menos cruces

    return [x for x in reversed(R)]  # debido a que se saca de R, se invierte

def sugiyama(G: nx.DiGraph) -> Tuple[nx.DiGraph, List[List[str]], Dict[str,int]]:
    S = cycleAnalysis(G)
    N = invertBackEdges(G, S)   # No es necesario guardar las aristas invertidos, simplemente se utiliza el grafo original.
    L = levelAssignment(N)
    N, L_b, node2level,between = getInBetweenNodes(G, L)    # No hay bordes de retroceso en N
    R = twoLevelCrossMin(N, L,node2level, between)
    return N, R, between



def nodes_to_coordinates(levels: List[List[str]]) -> Dict[str, tuple]:
    """
    Convierte los nodos en coordenadas (índices en ambas listas de niveles).

    Args:
    - levels: Lista de listas que representa los niveles del grafo.

    Returns:
    - Un diccionario que mapea cada nodo a su coordenada (índice en ambas listas).
    """
    coordinates = {}  # Diccionario para almacenar las coordenadas de los nodos
    for i, level in enumerate(levels):
        for j, node in enumerate(level):
            coordinates[node] = (i, j)  # Asigna la coordenada del nodo
    return coordinates



def draw_smooth_lines(ax, points, down_edge, up_edge, node_size=0.2,colorSalmon=False):
    """
    Dibuja líneas suavizadas entre puntos en un eje dado de Matplotlib,
    teniendo en cuenta el tamaño de los nodos para evitar solapamientos.

    Args:
        ax (matplotlib.axes._axes.Axes): Eje de Matplotlib en el que se dibujará.
        points (list of tuples): Lista de tuplas que contienen coordenadas (x, y).
        node_size (int): Tamaño de los nodos en el gráfico.

    Returns:
        None
    """
    # Extrae las coordenadas x e y de los puntos
    x_coords, y_coords = zip(*points)

    # Genera más puntos para una curva más suave
    x_interp = np.linspace(min(x_coords), max(x_coords), 300)
    spl = make_interp_spline(x_coords, y_coords, k=min(3,len(x_coords)-1)) 
    y_interp = spl(x_interp)

    # Dibuja los puntos interpolados
    ax.plot(x_interp, y_interp, color='salmon' if colorSalmon else 'skyblue')

    # Calcula la dirección de la línea utilizando dos puntos
    direction_start = np.array([x_interp[1], y_interp[1]]) - np.array([x_interp[0], y_interp[0]])
    direction_end = np.array([x_interp[-1], y_interp[-1]]) - np.array([x_interp[-2], y_interp[-2]])

    # Normaliza las direcciones
    direction_start /= np.linalg.norm(direction_start)*100
    direction_end /= np.linalg.norm(direction_end)*100

    # Ajusta los puntos de inicio y fin para evitar solapamientos con los nodos
    node_radius = node_size / 2  # Radio del nodo
    if np.linalg.norm(direction_start) < node_radius:
        direction_start *= (node_radius / np.linalg.norm(direction_start))
    if np.linalg.norm(direction_end) < node_radius:
        direction_end *= (node_radius / np.linalg.norm(direction_end))

    # Distancia entre el nodo y el punto de inicio y fin de la línea
    dist_start = np.sqrt(direction_start[0] ** 2 + direction_start[1] ** 2)
    dist_end = np.sqrt(direction_end[0] ** 2 + direction_end[1] ** 2)

    # Añade flechas a ambos lados de la línea, ajustando el punto de inicio y fin
    if down_edge:
        ax.annotate('', xy=(x_interp[0]+direction_start[0], y_interp[0] + direction_start[1]), xytext=(x_interp[0]+direction_start[0]*0.5 , y_interp[0]+direction_start[1]*0.5),
                    arrowprops=dict(facecolor='black', arrowstyle='<-'))
    if up_edge:
        ax.annotate('', xy=(x_interp[-1] - direction_end[0] , y_interp[-1] - direction_end[1]), xytext=(x_interp[-1]- direction_end[0]*0.5, y_interp[-1] - direction_end[1]*0.5),
                    arrowprops=dict(facecolor='black', arrowstyle='<-'))

def getPathEdges(edge, artificial_nodes,end_path_edges,pos):
    """
    Obtiene los bordes de la ruta entre nodos dados.

    Args:
        edge (tuple): Borde actual entre nodos.
        artificial_nodes (lista de tuples): Lista de nodos artificiales.
        end_path_edges (lista de tuples): Lista de bordes finales de la ruta.
        pos (dict): Diccionario de posiciones de nodos.

    Returns:
        list: Lista de coordenadas de los bordes de la ruta.
    """
    finished = False
    preFinishingNodes = list(map(lambda x: x[1],end_path_edges))
    current_u, current_v = edge
    result = [pos.get(current_u),pos.get(current_v)]
    edges = [edge]
    while not finished:
        current_u, current_v = [e for e in artificial_nodes+end_path_edges if e[0]==current_v][0]
        edges.append((current_u,current_v))
        result.append(pos.get(current_v))
        finished = current_v in preFinishingNodes
    return result,edges


def plotGraphWithLevels(G: nx.DiGraph, levels: List[List[str]], node_positions: Dict[str, tuple], ogGraph: nx.DiGraph, ax=None, between=[], edge_colors: Dict[tuple, str] = None) -> None:
    """
    Trazar el grafo con niveles utilizando las posiciones de los nodos.

    Args:
    - G: El grafo DiGraph.
    - levels: Lista de listas que representa los niveles del grafo.
    - node_positions: Diccionario que mapea cada nodo a su posición (coordenada).
    - ax: Eje en el que se trazará el gráfico. Si es None, se creará uno nuevo.
    - between: Lista de nodos "between" que se deben manejar de manera especial.
    - edge_colors: Diccionario que mapea cada arista a su color.
    
    Returns:
    - None
    """
    if ax is None:
        fig, ax = plt.subplots()  # Crear una nueva figura y eje si no se proporciona uno
    
    # Eliminar los nodos "between" de la lista de nodos
    nodes_to_draw = [node for node in G.nodes if node not in between]
    initial_path_edges = [e for e in G.edges if not e[0] in between and e[1] in between]
    end_path_edges = [e for e in G.edges if e[0] in between and not e[1] in between]
    artificial_nodes = [e for e in G.edges if e[0] in between and e[1] in between]
   
    # Dibujar las aristas de los nodos between
    for edge in initial_path_edges:
        points, edges = getPathEdges(edge, artificial_nodes, end_path_edges, node_positions)
        up_edge = (edges[0][0], edges[-1][1]) in ogGraph.edges
        down_edge = (edges[-1][1], edges[0][0]) in ogGraph.edges
        draw_smooth_lines(ax, points, down_edge, up_edge,colorSalmon="defini" in edge[0].lower())

    # Preparar colores para las aristas
    if edge_colors is None:
        edge_color = ["black" for e in G.edges()]
    else:
        edge_color = [edge_colors.get(e, "black") for e in G.subgraph(nodes_to_draw).edges()]

    # Dibujar el grafo sin los nodos "between"
    nx.draw_networkx_edges(G.subgraph(nodes_to_draw), pos=node_positions, edge_color=edge_color, ax=ax)
    nx.draw_networkx_nodes(G.subgraph(nodes_to_draw), pos=node_positions, ax=ax, node_size=500)
    nx.draw_networkx_labels(G.subgraph(nodes_to_draw), pos=node_positions, ax=ax)


    # Ajustar los ejes para que todas las etiquetas de nodo sean visibles
    #ax.set_aspect('auto')
    #ax.autoscale(enable=True, axis='both', tight=True)
if __name__ == '__main__':
    # Define las aristas del grafo
    edges = [('A', 'B'), ('A', 'E'), ('B', 'C'), ('C', 'D'), ('D', 'C'), ('A', 'C')]
    G = nx.DiGraph(edges)

    # Ejecuta el algoritmo sugiyama
    N:nx.DiGraph = None
    N , R, between = sugiyama(G)
    
    # Trazar ambos grafos en un subplot
    fig, axes = plt.subplots(1, 2, figsize=(12, 6))  # Crear una figura con dos subplots
    pos = nx.planar_layout(G)  # Calcula la disposición de los nodos para el grafo original
    nx.draw(G, pos=pos, ax=axes[0],with_labels=True, node_size=500, arrowsize=10)  # Trazar el grafo original en el primer subplot
    coordinates = nodes_to_coordinates(R)  # Obtener las coordenadas de los nodos en el grafo resultante
    pos = {node: coordinates[node] for node in N.nodes}  # Obtener las posiciones de los nodos en el grafo resultante
    plotGraphWithLevels(N, R, pos, G, ax=axes[1], between=between)  # Trazar el grafo resultante con niveles en el segundo subplot
    plt.tight_layout()
    plt.show()
    print("espera")
